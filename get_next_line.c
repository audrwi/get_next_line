/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 19:55:55 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/06 15:36:17 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char		*stop_string(char *str)
{
	size_t		i;

	i = 0;
	while (str[i] && str[i] != '\n')
		i++;
	if (str[i])
		str[i] = '\0';
	return (str);
}

static int		final_return(char **line, int ret)
{
	if (*line && *line[0])
		return (1);
	free(*line);
	*line = NULL;
	return (ret < 0 ? -1 : 0);
}

int				get_next_line(int const f, char **l)
{
	int			r;
	static char	*e[256];
	char		*t;
	char		b[BUFF_SIZE + 1];

	if (f < 0 || f > 256 || !l || BUFF_SIZE < 0)
		return (-1);
	if (e[f] && (t = ft_strchr(e[f], '\n')))
	{
		*l = ft_strdup(stop_string(e[f]));
		return ((e[f] = t[1] ? ft_strdup((char *)&t[1]) : NULL) - e[f] + 1);
	}
	*l = e[f] ? ft_strdup(e[f]) : ft_strdup("");
	e[f] = NULL;
	while ((r = read(f, b, BUFF_SIZE)) > 0)
	{
		b[r] = '\0';
		if ((t = ft_strchr(b, '\n')))
		{
			*l = ft_strjoinf(*l, stop_string(b), 1);
			return ((e[f] = t[1] ? ft_strdup((char *)&t[1]) : NULL) - e[f] + 1);
		}
		*l = ft_strjoinf(*l, b, 1);
	}
	return (final_return(l, r));
}

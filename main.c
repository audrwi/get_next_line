/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 20:12:03 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/06 19:04:52 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static void	error(int rslt)
{
	if (rslt == -1)
		ft_putendl_fd("\033[0;31merror\033[0m: something went wrong", 2);
}

static void	test(int fd)
{
	int		rslt;
	char	*line;

	line = NULL;
	while ((rslt = get_next_line(fd, &line)) > 0)
	{
		ft_putendl(line);
		free(line);
	}
	error(rslt);
}

int			main(int ac, char **av)
{
	int		i;
	int		fd;
	int		fd2;
	int		rslt;
	char	*line;

	if (ac == 2)
		test(open(av[1], O_RDONLY));
	else if (ac == 3)
	{
		i = 0;
		line = NULL;
		fd = open(av[1], O_RDONLY);
		fd2 = open(av[2], O_RDONLY);
		while ((rslt = get_next_line(i % 2 == 0 ? fd : fd2, &line)) > 0)
		{
			ft_putendl_fd(line, i % 2 + 1);
			free(line);
			i++;
		}
		error(rslt);
	}
	else
		test(0);
	return (0);
}

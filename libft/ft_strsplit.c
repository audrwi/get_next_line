/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 12:27:33 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/07 20:59:51 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	**create_word(char **tab, char *str, int rows, char c)
{
	int		i;
	int		j;
	size_t	cols;

	i = 0;
	j = 0;
	while (j < rows)
	{
		cols = 0;
		while (str[i] && str[i] != c)
		{
			cols++;
			i++;
		}
		while (str[i] == c)
			i++;
		tab[j] = ft_strnew(cols);
		if (tab[j] == NULL)
			return (NULL);
		j++;
	}
	return (tab);
}

static char	**fill_word(char **tab, char *str, int rows, char c)
{
	int		i;
	int		j;
	int		cols;

	i = 0;
	j = 0;
	while (j < rows)
	{
		cols = 0;
		while (str[i] && str[i] != c)
		{
			tab[j][cols] = str[i];
			cols++;
			i++;
		}
		while (str[i] == c)
			i++;
		j++;
	}
	tab[j] = NULL;
	return (tab);
}

char		**ft_strsplit(char const *s, char c)
{
	int		i;
	int		rows;
	char	*str;
	char	**tab;

	i = 0;
	rows = 1;
	str = ft_strtrimc(s, c);
	if (!str || (str && !str[0]))
	{
		if ((tab = (char **)malloc(sizeof(char *))) == NULL)
			return (NULL);
		tab[0] = NULL;
		return (tab);
	}
	while (str[i])
	{
		if (str[i] == c && str[i + 1] != c && str[i + 1])
			rows++;
		i++;
	}
	if ((tab = (char **)malloc(sizeof(char *) * (rows + 1))) == NULL)
		return (NULL);
	tab = create_word(tab, str, rows, c);
	return (fill_word(tab, str, rows, c));
}
